//When creating a react component, we first import react as a package.
//However, since fragments no longer need to be coming from React, we won't need to import anymore. This is because in the latest update of React, React is no longer needed to be import to create your components. However, if there are components from React that you need to use, you should still import React.
//import is similar to require() in expressjs.
//import React from 'react';

import {useState,useEffect} from 'react';

//import react-bootstrap components
import {Container} from 'react-bootstrap';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'

//import components - to use components in another component/page, first import them.
import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import AddCourse from './pages/AddCourse';
import Logout from './pages/Logout';
import ViewCourse from './pages/ViewCourse';

import { UserProvider } from './userContext';


//import css
import './App.css';

//Components should be able to return something or react elements.
//If you want to return a blank page, return null.
//Even in your components, add react fragments when returning 2 or more adjacent elements.
//Import and render/return components inside other components.
export default function App(){

  const [user,setUser] = useState({

      id: null,
      isAdmin: null,

  })

  useEffect(()=>{

    fetch('http://localhost:4000/users/getUserDetails',{

      method: 'GET',
      headers: {

        'Authorization': `Bearer ${localStorage.getItem('token')}`

      }
    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  },[])

 const unsetUser = () => {

  localStorage.clear()

 }

  return (
  
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courses/viewCourse/:courseId" element={<ViewCourse />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/addcourse" element={<AddCourse />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<ErrorPage />} />
           </Routes>
         </Container>
        </Router>
      </UserProvider>
    </>

  )

}
