import {useState,useEffect,useContext} from 'react';

//import the Banner Component
import Banner from '../components/Banner';
//import the CourseCard component
import CourseCard from '../components/Course';
import AdminDashboard from '../components/AdminDashboard';

import UserContext from '../userContext';

export default function Courses(){

	const {user} = useContext(UserContext);

	let coursesBanner = {

		title: "Welcome to the Courses Page.",
		description: "View one of our courses below.",
		buttonText: "Register/Login to Enroll",
		destination: "/register"

	}

	const [coursesArray,setCoursesArray] = useState([])

	useEffect(()=>{

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data =>{

			//console.log(data);

			setCoursesArray(data.map(course => {

				return (

					<CourseCard key={course._id} courseProp={course} />

					)
			}))

		})

	},[])

	/*
		Mini-Activity:

		Pass the coursesBanner object to our Banner component as props.

		Check the object and debug if necessary.

		Take a screenshot of your page and send it in the hangouts.

	*/

	/*
		Components are independent from one another. It may come from the same component file but when returned each component is independent from one another. 

		Remember, components are functions. We run the same function multiple times.

	*/

	//let propCourse1 = coursesData[0];
	//let propCourse2 = "sample data 2";
	//let propCourse3 = "sample data 3";

	/*
		Display the first item in the coursesData array in our first CourseCard component.

		Mini-Activity:

		Pass the 2nd item of the coursesData array into our 2nd CourseCard Component.
		Pass the 3rd item of the coursesData array into our 3rd CourseCard Component.

		Create/add a new course in the coursesData array in courses.js.
			-id should be wdc004
			-Add your own course name
			-Add lorem ipsum as description
			-Add your own price 
			-Add onOffer property but the value is false.

		In your Courses page component, add another CourseCard component.

		Pass the 4th item of the coursesData array into our 4th CourseCard Component.

		Take a screenshot of your output and send it in the hangouts.

	*/

	/*Create an array of CourseCard components by mapping out details from our coursesData array*/
	/*let coursesComponents = coursesData.map(course => {

		//we essentially returned an array of CourseCard components into our new courseComponents.

		//We are also able to pass the data of the current item being looped by map(), by passing the course parameter in our anonymous function

		//When we create an array of components, we have to add a unique identifier for each component. Pass a key prop with a unique identifier for each item in the array

		return <CourseCard courseProp={course} key={course.id}/>

	})*/

	/*console.log(coursesComponents)*/


	return (

		user.isAdmin
		?
		<AdminDashboard />
		:

		<>
			<Banner bannerProp={coursesBanner}/>
			{coursesArray}

		</>


	)

}